
var app =require('express');
var http = require('http').createServer(app);
var io = require('socket.io').listen(http);
var td = require('./toDT.js');
var cio = require('socket.io-client');
var Io = cio.connect('http://89.218.233.62:3001');
var ami = require('asterisk-manager')('5038','localhost','3gpastat-ami','af3e4148351c0a90ace0adb417e15716', true);
ami.connect();
var mysql = require('mysql');
io.on('connection' , function(soIO) {
    soIO.on("requestJoin", function(room) {        
        soIO.join(room);
    });
    soIO.on('amiQueJoin' , function(data) {
        io.emit('setQue' , data);
    });
    soIO.on('amiQueLeave' , function(data) {
        io.emit('delQue' , data);
    });
    soIO.on('amiExt' , function(data) {
        io.emit('dcs' , data);
    });
});

var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database:'cmonitor'
});

con.connect(function(err) {
  if (err) throw err;
  console.log("Connected!");
});
con.config.queryFormat = function (query, values) {
    if (!values) return query;
    return query.replace(/\:(\w+)/g, function (txt, key) {
      if (values.hasOwnProperty(key)) {
        return this.escape(values[key]);
      }
      return txt;
    }.bind(this));
  };
var start = function(evt) {
    Io.emit('requestJoin' , {msg:'msh'});
    ami.on('queuecallerjoin' , function (evt) {
        var query = con.query('INSERT INTO queue(number , dtb , position, queuenum,uid) values (:n,:d,:p,:qn ,:u)' , {
            n:evt.calleridnum,
            d:td(new Date()),
            p:evt.position,
            qn:evt.queue,
            u:evt.linkedid
        } , function(err ,res){
            if(err) throw error;
            Io.emit('amiQueJoin' , {number:evt.calleridnum , queue:evt.queue , uid:evt.linkedid , dtb:td(new Date())});
        });
        console.log(evt);
        
    });
    ami.on('queuecallerleave' , function (evt) {
        console.log('\n***LEAVE***');
        console.log(evt)
        var query = con.query('DELETE from queue WHERE uid=:u' , {u:evt.linkedid} , function(err ,res) {
            console.log(err);
            if(err) throw error;
            Io.emit('amiQueLeave' , {uid:evt.linkedid ,  num:evt.connectedlinenum ,caller:evt.calleridnum});
        });
        console.log(evt);
        var op = con.query("UPDATE operators SET caller = :c WHERE num=:n" , {c:evt.calleridnum , n:evt.connectedlinenum})
    });


    ami.on('extensionstatus' , function(evt) {
        if(evt.statustext!='InUse') {
            var q  = con.query('UPDATE operators set caller=NULL where num=:n' , {n:evt.exten} , function(err,res) {
                if (err) throw error;
            });
        }
        var query = con.query('UPDATE operators SET status=:s ,lastevent=:d where num=:n' , {s:evt.statustext , n:evt.exten,d:td(new Date()) }, function (err ,res) {
            if (err) throw error;
            Io.emit('amiExt' , {agent:evt.exten , status:evt.statustext , dtb:td(new Date())});
        }) 
        console.log(evt);
        
    });
}
ami.on('fullybooted' , start)

http.listen(3001);
