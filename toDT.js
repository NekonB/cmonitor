
module.exports = function (date) {
    Date.prototype.toLocalISOString = function() {
    
        // Helper for padding
        function pad(n, len) {
          return ('000' + n).slice(-len);
        }
      
        // If not called on a Date instance, or timevalue is NaN, return undefined
        if (isNaN(this) || Object.prototype.toString.call(this) != '[object Date]') return;
      
        // Otherwise, return an ISO format string with the current system timezone offset
        var d = this;
        var os = d.getTimezoneOffset();
        var sign = (os > 0? '-' : '+');
        os = Math.abs(os);
        
        return pad(d.getFullYear(), 4) + '-' +
               pad(d.getMonth() + 1, 2) + '-' +
               pad(d.getDate(), 2) +
               'T' + 
               pad(d.getHours(), 2) + ':' +
               pad(d.getMinutes(), 2) + ':' +
               pad(d.getSeconds(), 2) 
             
               // Note sign of ECMASCript offsets are opposite to ISO 8601
    }

    var dt = date.toLocalISOString().replace('T', ' ');
    return dt;
}